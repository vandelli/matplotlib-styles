# Matplotlib Styles

This is a collection of Matplotlib style files. In order to use them see the [Matplotlib style guide](http://matplotlib.org/users/style_sheets.html#defining-your-own-style). Quick guide for the lazy ones:

### Directly from gitlab
```python
import matplotlib.pyplot as plt
plt.style.use('https://gitlab.cern.ch/vandelli/matplotlib-styles/raw/master/styles/ATLAS.mplstyle')
```

### From a local repository
```sh
git clone https://:@gitlab.cern.ch:8443/vandelli/matplotlib-styles.git
```
```python
import matplotlib.pyplot as plt
plt.style.use('matplotlib-styles/styles/ATLAS.mplstyle')
```

### From Matplotlib configuration directory
```sh
git clone https://:@gitlab.cern.ch:8443/vandelli/matplotlib-styles.git
mkdir -p ~/.config/matplotlib/stylelib
cp matplotlib-styles/styles/*.mplstyle ~/.config/matplotlib/stylelib/
```
```python
import matplotlib.pyplot as plt
plt.style.use('ATLAS')
```

